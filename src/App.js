import {BrowserRouter, Route} from "react-router-dom";
import PromoBar from "./components/PromoBar";
import FooterArea from "./components/FooterArea";
import HomePage from "./components/HomePage";
import CartPage from "./components/CartPage";
import NaviBar from "./components/NaviBar";
import React from "react";

function App() {
  return (
    <div>
        <BrowserRouter>
            <PromoBar/>
            <NaviBar/>
            <div>
                <Route path="/" exact component={HomePage}/>
                <Route path="/cart" exact component={CartPage}/>
            </div>
            <FooterArea/>
        </BrowserRouter>
    </div>
  );
}

export default App;
