export const BASE_URL = "http://api-ecommerce.mark2win.com/product";

export const ACTION_TYPES = {
    FETCH_CHAIR_DATA: "FETCH_CHAIR_DATA",
    ADD_TO_CART: "ADD_TO_CART",
    INCREMENT: "INCREMENT",
    DECREMENT: "DECREMENT",
    REMOVE: "REMOVE"
}



