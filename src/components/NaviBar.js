import React, {Component} from "react";
import "../HMChairs.css";
import logo from "../assets/HMlogo.png";
import ShoppingCart from "./ShoppingCart";

class NaviBar extends Component {
    render() {
        return (
            <div className="naviBar">
                <div className="naviBarLeft">
                    <img src={logo} className="logo" alt="logo"/>
                    <div className="search">
                        <i className="fas fa-search"/>
                        <input className="searchBar" type="search" placeholder="Search"/>
                    </div>

                </div>
                <div style={{width: "60%"}}>
                    <div className="naviBarCenter">
                        <span>Office Chairs</span>
                        <span>Work From Home</span>
                        <span>Gaming</span>
                        <span>Furniture</span>
                        <span>Lighting</span>
                        <span>Decor</span>
                    </div>
                </div>
                <div className="naviBarRight">
                    <i className="fas fa-user"/>
                    <i className="fas fa-heart"/>
                    <ShoppingCart/>
                </div>
            </div>
        )
    }
}

export default NaviBar;