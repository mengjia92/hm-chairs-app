import ChairListHeader from "./ChairListHeader";
import ChairListContent from "./ChairListContent";

function HomePage() {
    return (
        <div>
            <ChairListHeader/>
            <ChairListContent/>
        </div>
    )
}

export default HomePage;