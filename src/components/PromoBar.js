import React, {Component} from "react";
import "../HMChairs.css";

class PromoBar extends Component {
    render() {
        return (
            <div className="promoBar">
                <span style={{width: "33.33%"}}/>
                <span className="promoInfo">Free Shipping on Performance Seating</span>
                <span className="support">
                    REGIONS
                    <span style={{margin: "0 8px"}}>-</span>
                    LOCATIONS
                    <span style={{margin: "0 8px"}}>-</span>
                    SUPPORT
                </span>
            </div>
        )
    }
}

export default PromoBar;