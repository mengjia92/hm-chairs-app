import React, {Component} from "react";
import "../HMChairs.css";

class ChairListHeader extends Component {
    render() {
        return (
            <div className="contentHeader">
                <div className="listCategory">
                    <span>Home > Office > Office Chairs</span>
                </div>
                <h1>Office Chairs</h1>
                <div className="optionBar">
                    <div className="optionBarLeft">
                        <h3>Price</h3>
                        <h3>Material</h3>
                        <div className="sortArea">
                            <h3>Sort By:</h3>
                            <button className="dropMenu">
                                <span style={{paddingRight: "5px"}}>Featured Product</span>
                                <i className="fas fa-caret-down"/>
                            </button>
                        </div>
                    </div>
                    <h4>showing 10 of 10 items</h4>
                </div>
            </div>
        )
    }
}

export default ChairListHeader;